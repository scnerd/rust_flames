use rayon::prelude::*;

struct FlameFiller {
    flame: &FlameBuffer,
    points: Vec<Point>,
    step: usize,
    ignore_steps: usize,
}

impl FlameFiller {
    fn new(flame: &mut FlameBuffer, num_points: usize, ignore_steps: usize) -> FlameFiller {
        let points = Range(num_points).into_iter().map(|_| Point::random()).collect()
        FlameFiller {
            flame,
            points,
            step: 0,
            ignore_steps,
        }
    }

    fn step(&mut self) {
        self.points = self.points.into_par_iter().map(self.flame.definition.apply).collect();
        self.step += 1;
        if self.step > self.ignore_steps {
            self.flame.draw(&self.points)
        }
    }

    fn run(&mut self, steps: usize) {
        Range(steps).into_iter().for_each(move |_| self.step());
    }
}
