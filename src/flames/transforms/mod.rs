use crate::flames::Point;
use nalgebra::Matrix3;

mod base;

trait TransformBase {
    fn transform_single(&self, point: Point) -> Point;
}

trait SimpleTransformBase : TransformBase {
    fn transform_single_position(&self, position: Vector3<f32>) -> Vector3<f32>;

    fn transform_single(&self, point: Point) -> Point {
        Point {
            position: self.transform_single_position(point.position),
            .. point
        }
    }
}
