use crate::{SimpleTransformBase, TransformBase};

struct StackedTransform<A: TransformBase, B: TransformBase> (A, B);

impl<B: TransformBase> Add for StackedTransform {
    // Return a stacked transform that runs each of the added transforms in series
    type Output = StackedTransform<Self, B>;

    fn add(self, other: B) -> Self::Output {
        StackedTransform::new(self, other)
    }
}

impl<A: TransformBase, B: TransformBase> TransformBase for StackedTransform<A, B> {
    fn transform_single(&self, point: Point) -> Point {
        let intermediate = self.0.transform_single(point);
        self.1.transform_single(intermediate)
    }
}

struct Transform<T: SimpleTransformBase> {
    pre: Matrix3<f32>,
    post: Matrix3<f32>,
    color: Vector3<f32>,
    transform: T,
    recolor_rate: f32,
}

impl<T: SimpleTransformBase> TransformBase for Transform<T> {
    fn transform_single(&self, point: Point) -> Point {
        let position1 = point.position;
        let position2 = &self.pre @ position1;
        let position3 = self.transform.transform_single_position(position2);
        let position = &self.post @ position3;

        let color = self.color * self.recolor_rate + point.color * (1 - self.recolor_rate);

        Point {
            position,
            color,
        }
    }
}
