#[derive(Debug, Default, Serialize, Deserialize)]
struct TransformSquareRoot {}

impl SimpleTransformBase for TransformSquareRoot {
    fn transform_single_position(&self, position: Vector3<f32>) -> Vector3<f32> {
        Vector3::new([
            position[0].powf(1_f32 / 2_f32),
            position[1].powf(1_f32 / 2_f32),
            position[2].powf(1_f32 / 2_f32),
        ])
    }
}

#[derive(Debug, Default, Serialize, Deserialize)]
struct TransformCubeRoot {}

impl SimpleTransformBase for TransformCubeRoot {
    fn transform_single_position(&self, position: Vector3<f32>) -> Vector3<f32> {
        Vector3::new([
            position[0].powf(1_f32 / 3_f32),
            position[1].powf(1_f32 / 3_f32),
            position[2].powf(1_f32 / 3_f32),
        ])
    }
}

