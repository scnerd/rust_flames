#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FlameDefinition<T> where T: TransformBase + Clone + Serialize + Deserialize {
    transforms: Vec<T>,
}

impl FlameDefinition {
    pub fn apply(&self, pt: Point) {
        let mut _pt = pt;
        // TODO: Can be replaced with a reduce
        for transform in self.transforms {
            _pt = transform.transform_single(_pt);
        }
        _pt
    }
}
