pub struct RasterGrid {
    pub bounds: Matrix2<f32>,
    pub resolution: Vector2<usize>,
    translation: Matrix3<f32>,
}

impl RasterGrid {
    pub fn new(x_min: f32, x_max: f32, x_res: usize, y_min: f32, y_max: f32, y_res: usize) -> Self {
        RasterGrid {
            bounds: Matrix2::new([
                [x_min, x_max],
                [y_min, x_max],
            ]),
            resolution: Vector2::new([x_res, y_res]),
            translation: Matrix3::new([
                [1_f32 / x_max, 0_f32, -(x_min - 0.5) / x_max],
                [0_f32, 1_f32 / y_max, -(y_min - 0.5) / y_max],
                [0_f32, 0_f32, 1_f32]
            ])
        }
    }

    pub fn rasterize(&self, pt: &Vector2<f32>) -> Vector2<usize> {
        (&self.translation @ pt).map(|v| v as usize)
    }
}
