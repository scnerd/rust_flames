#[derive(Debug, Clone)]
pub struct FlameBuffer {
    pub grid: RasterGrid,
    pub bins: Matrix<f32>,  // Should be HxWx3, I don't think this is quite right
    pub definition: &FlameDefinition,
}

impl FlameBuffer {
    pub fn new(definition: &FlameDefinition, grid: RasterGrid) -> Self {
        Self {
            grid,
            bins: Matrix::zeros((grid.resolution.0, grid.resolution.1, 3)),
            definition,
        }
    }

    pub fn draw(&mut self, points: &Vec<Point>) {
        points.iter().map(|pt|
            Point {
                position: self.grid.rasterize(pt.position),
                .. pt
            }
        ).for_each(|pt| {
            self.bins[[pt.position[0], pt.position[1]]] += pt.color;
        })
    }
}
