use iced::executor;
use iced::theme::{self, Theme};
use iced::time;
use iced::widget::{
    button, checkbox, column, container, pick_list, row, slider, text,
};
use iced::window;
use iced::{
    Alignment, Application, Command, Element, Length, Settings, Subscription,
};

use std::time::{Duration, Instant};

pub fn main() -> iced::Result {
    tracing_subscriber::fmt::init();

    Flames::run(Settings {
        antialiasing: true,
        window: window::Settings {
            position: window::Position::Centered,
            ..window::Settings::default()
        },
        ..Settings::default()
    })
}


#[derive(Default)]
struct Flames {
    // grid: Grid,
    // is_playing: bool,
    // queued_ticks: usize,
    // speed: usize,
    // next_speed: Option<usize>,
    // version: usize,
}

#[derive(Debug, Clone)]
enum Message {
    // Grid(grid::Message, usize),
    // Tick(Instant),
    // TogglePlayback,
    // ToggleGrid(bool),
    // Next,
    // Clear,
    // SpeedChanged(f32),
    // PresetPicked(Preset),
}

impl Application for Flames {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();


    fn new(_flags: ()) -> (Self, Command<Message>) {
        (
            Self {
                // speed: 5,
                // ..Self::default()
            },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("Fractal Flames")
    }

    fn update(&mut self, message: Message) -> Command<Message> {
        match message {
            // Message::Grid(message, version) => {
            //     if version == self.version {
            //         self.grid.update(message);
            //     }
            // }
            // Message::Tick(_) | Message::Next => {
            //     self.queued_ticks = (self.queued_ticks + 1).min(self.speed);
            //
            //     if let Some(task) = self.grid.tick(self.queued_ticks) {
            //         if let Some(speed) = self.next_speed.take() {
            //             self.speed = speed;
            //         }
            //
            //         self.queued_ticks = 0;
            //
            //         let version = self.version;
            //
            //         return Command::perform(task, move |message| {
            //             Message::Grid(message, version)
            //         });
            //     }
            // }
            // Message::TogglePlayback => {
            //     self.is_playing = !self.is_playing;
            // }
            // Message::ToggleGrid(show_grid_lines) => {
            //     self.grid.toggle_lines(show_grid_lines);
            // }
            // Message::Clear => {
            //     self.grid.clear();
            //     self.version += 1;
            // }
            // Message::SpeedChanged(speed) => {
            //     if self.is_playing {
            //         self.next_speed = Some(speed.round() as usize);
            //     } else {
            //         self.speed = speed.round() as usize;
            //     }
            // }
            // Message::PresetPicked(new_preset) => {
            //     self.grid = Grid::from_preset(new_preset);
            //     self.version += 1;
            // }
        }

        Command::none()
    }

    fn subscription(&self) -> Subscription<Message> {
        // if self.is_playing {
        //     time::every(Duration::from_millis(1000 / self.speed as u64))
        //         .map(Message::Tick)
        // } else {
        //     Subscription::none()
        // }
        Subscription::none()
    }

    fn view(&self) -> Element<Message> {
        let controls = view_controls(
        );

        let content = column![
            self.grid
                .view()
                .map(move |message| Message::Grid(message, version)),
            controls,
        ];

        container(content)
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }

    fn theme(&self) -> Theme {
        Theme::Dark
    }
}

fn view_controls<'a>(
) -> Element<'a, Message> {
    row![
    ]
    .padding(10)
    .spacing(20)
    .align_items(Alignment::Center)
    .into()
}
