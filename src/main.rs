mod flames;

fn main() {
    let flame_definition = FlameDefinition {
        transforms: vec![
            TransformSquareRoot::random(),
            TransformCubeRoot::random(),
        ],
    };
    let grid = RasterGrid::new(
        -2_f32, 2_f32, 32,
        -1_f32, 1_f32, 16,
    );

    let buffer = FlameBuffer::new(
        flame_definition,
        grid,
    );
    let filler = FlameFiller::new(
        buffer,
        1000,
        1,
    );
    filler.step();
    filler.step();
    filler.step();
}